
//
//  DemoScene.cpp
//  collideDemo
//
//  Created by Yiguang Lu on 12-5-16.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include "DemoScene.h"
#include "BallSpriteWithBody.h"
#include "DirtSpriteWithBody.h"
#include "AtomSpriteWithBody.h"
#include "Bomb.h"
#include "Box2DUtil.h"
#include "SpriteBase.h"
#include "GameOverMenuLayer.h"
#include "Menu.h"
#include "SimpleAudioEngine.h"

CCScene* CDemoScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::node();
    
    // add layer as a child to scene
    CCLayer* layer = new CDemoScene();
    scene->addChild(layer);
    layer->release();
    
    return scene;
}



void CDemoScene::didAccelerate(CCAcceleration* acceleration)
{
    if(enteredFinished)
        return;
    float deceleration = 0.2; 
    
    // -- determins how sensitive the accelerometer reacts(higher = more sensitive)
    float sensitivity = 30.0;
    
    // -- how fast the velocity can be at most
    float maxVelocity = 1000;
    
    CCPoint offsetVector;
    
    float offsetX = offsetVector.x * (sensitivity * ( 1 * -1 ));
    float offsetY = offsetVector.y * (sensitivity * ( 1 * -1 ));
    
    //float movement = (acelx * sensitivity) + offset;
    
    
    // adjust velocity based on current accelerometer acceleration
    playerVelocity.x = playerVelocity.x * deceleration + acceleration->x * sensitivity + offsetX;
    playerVelocity.y = playerVelocity.y * deceleration + acceleration->y * sensitivity + offsetY;
    
    
    // -- we must limit the maximum velocity of the player sprite, in both directions
    if (playerVelocity.x > maxVelocity)
    {
        playerVelocity.x = maxVelocity;
    }
    else if (playerVelocity.x < - maxVelocity)
    {
        playerVelocity.x = - maxVelocity;
    }
    
    if (playerVelocity.y > maxVelocity)
    {
        playerVelocity.y = maxVelocity;
    }
    else if (playerVelocity.y < - maxVelocity)
    {
        playerVelocity.y = - maxVelocity;
    }

    hero->setGravity(playerVelocity);
    
}

void CDemoScene::updateAccelerometer(ccTime dt)
{
    // -- keep adding up the playerVelocity to the player's position
//    CCPoint pos = hero->getPosition();
//
//    pos.x +=  playerVelocity.x;
//    pos.y +=  playerVelocity.y;
//    
//    
//    // -- The player should also be stopped from going outside the screen
//    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
//    float imageWidthHalved = 32;
//    float leftBorderLimit = imageWidthHalved - 2;
//    float rightBorderLimit = screenSize.width - imageWidthHalved;
//    float topBorderLimit = screenSize.height - imageWidthHalved;
//    float bottomBorderLimit = imageWidthHalved;
//    
//    // -- preventing the player sprite from moving outside the screen
//    if (pos.x < leftBorderLimit)
//    {
//        pos.x = leftBorderLimit;
//        playerVelocity = CCPointZero;
//    }
//    else if (pos.x > rightBorderLimit)
//    {
//        pos.x = rightBorderLimit;
//        playerVelocity = CCPointZero;
//    }
//    
//    if (pos.y < bottomBorderLimit)
//    {
//        pos.y = bottomBorderLimit;
//        playerVelocity = CCPointZero;
//    }
//    else if (pos.y > topBorderLimit)
//    {
//        pos.y = topBorderLimit;
//        playerVelocity = CCPointZero;
//    }
//    
//    
//    // assigning the modified position back
//    hero->setPosition(pos);
//    Environment::cat = pos;
}


CDemoScene::CDemoScene()
{
    
    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
    
    CCSprite *background = CCSprite::spriteWithFile("bkg7.png");
	background->setPosition(ccp(screenSize.width*0.7, screenSize.height/2));
	background->setScale(0.8);
    background->setOpacity(128);
    
	this->addChild(background);
    
    {
        setIsTouchEnabled( true );
        setIsAccelerometerEnabled( true );
        
        enteredFinished = false;
        Environment::finished = false;
        SpriteBase::initFadeOutParameter();
        
         //UXLOG(L"Screen width %0.2f screen height %0.2f",screenSize.width,screenSize.height);
        
        Environment::cat = CCPointMake(screenSize.width / 2, screenSize.height / 2);
        
        Environment::GetSprites().clear();
        //littleDotVector.clear();
        
        CCDirector::sharedDirector()->setDepthTest(false);
        
                
        //    CCSprite *background = CCSprite::spriteWithFile("bk.jpg");
        //    this->addChild(background);
        //    background->setAnchorPoint(ccp(0,0));
        //    background->setScale(0.8);
        
        CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("balls.plist");
        
        CCSpriteBatchNode *batch = CCSpriteBatchNode::batchNodeWithFile("balls.png");
        
        this->addChild(batch,0);
        
        this->scheduleUpdate();
    }
    
    Environment::SetLayer(this);
    
    {
        // Define the gravity vector.
        b2Vec2 gravity;
        gravity.Set(0.0f, 0.0f);
        
        // Do we want to let bodies sleep?
        bool doSleep = true;
        
        // Construct a world object, which will hold and simulate the rigid bodies.
        Environment::SetWorld(new b2World(gravity));
        Environment::GetWorld()->SetAllowSleeping(doSleep); 
        Environment::GetWorld()->SetContinuousPhysics(true);
        
        addWalls();
        
        hero = new Hero(this);
        Environment::hero = hero;
    }
    
    scoreLabel = CCLabelBMFont::labelWithString("0", "testFont.fnt");
    
    scoreLabel->setColor( ccc3(253, 192,   5));
    
    scoreLabel->setBlendFunc((ccBlendFunc){GL_SRC_ALPHA, GL_ONE});
    
    scoreLabel->setAnchorPoint(ccp(1.0f,1.0f));
    
    scoreLabel->setPosition(ccp(screenSize.width, screenSize.height));
    
    score = 0;
    
    addChild(scoreLabel);
    
//    for (int i = 0; i < 500; i++)
//    {
//        Environment::GetSprites().push_back(new CAtomSpriteWithBody());
//    }
    
    
    CCLabelBMFont *label_back = CCLabelBMFont::labelWithString("Menu","testFont.fnt");
	label_back->setColor(ccc3(207,136,88));
	label_back->setScale(0.6);
	//this->addChild(label_back);
    
	CCMenuItemLabel *pBackItem = CCMenuItemLabel::itemWithLabel(
                                                                label_back,
                                                                this,
                                                                menu_selector(CDemoScene::menuBackCallback) );
	
	CCMenu* pMenu = CCMenu::menuWithItems(pBackItem, NULL);
	pMenu->setPosition(ccp(70,screenSize.height-10));
	this->addChild(pMenu, 1);
    
}


void CDemoScene::menuBackCallback(CCObject* pSender)
{
	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::transitionWithDuration(0.7f,Menu::scene()));
}

CDemoScene::~CDemoScene()
{
    //Fuck...Let the memeory leak!
    
//    list<SpriteBase*> &s = Environment::GetSprites();
//    while (!s.empty())
//    {
//        delete *(s.begin());
//        s.erase(s.begin());
//    }
    
    //delete hero;
    //delete Environment::GetWorld();
}

float sigma_time = 0;


void CDemoScene::updateScore()
{
    char buffer[64];
    sprintf(buffer,"%ld",score);
    scoreLabel->setString(buffer);
}

void CDemoScene::addWalls()
{
    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
    
    b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(screenSize.width/2/PTM_RATIO, 
                               screenSize.height/2/PTM_RATIO); // bottom-left corner
	
    
    b2World *world = Environment::GetWorld();
	// Call the body factory which allocates memory for the ground body
	// from a pool and creates the ground box shape (also from a pool).
	// The body is also added to the world.
	b2Body* groundBody = world->CreateBody(&groundBodyDef);
	
	// Define the ground box shape.
	b2PolygonShape groundBox;		
	
    // bottom
    groundBox.SetAsBox(screenSize.width/2/PTM_RATIO, 0, b2Vec2(0, -screenSize.height/2/PTM_RATIO), 0);
 	
    b2FixtureDef fixtureDef;
	fixtureDef.shape = &groundBox;	
	fixtureDef.density = 30.0f;
	fixtureDef.friction = 0.2f;
    fixtureDef.restitution = 0.2f;
    
    //here set it to collide with the dots
    fixtureDef.filter.maskBits = 0x0008;
    fixtureDef.filter.categoryBits = 0x00010;
    
    groundBody->CreateFixture(&fixtureDef);
	
    
    
    // top
    groundBox.SetAsBox(screenSize.width/2/PTM_RATIO, 0, b2Vec2(0, screenSize.height/2/PTM_RATIO), 0);
    groundBody->CreateFixture(&fixtureDef);
	
    // left
    groundBox.SetAsBox(0, screenSize.height/2/PTM_RATIO, b2Vec2(-screenSize.width/2/PTM_RATIO, 0), 0);
    groundBody->CreateFixture(&fixtureDef);
	
    // right
    groundBox.SetAsBox(0, screenSize.height/2/PTM_RATIO, b2Vec2(screenSize.width/2/PTM_RATIO, 0), 0);
    groundBody->CreateFixture(&fixtureDef);
	
	

}

void CDemoScene::update(ccTime delta)
{
    
    if (enteredFinished)
        return;
    
    static float scoreThing = 0;
    scoreThing += delta;
    if(scoreThing > 0.5)
    {
        scoreThing = 0;
        score++;
    }
    
    {
        sigma_time += delta;
        if (sigma_time > 0.00)
        {            
        }
        else 
        {
            return ;
        }
    }
    updateAccelerometer(delta);
    if (!Environment::finished)
    {
        list<SpriteBase*> &s = Environment::GetSprites();
        for (list<SpriteBase*>::iterator it = s.begin(); it != s.end(); )
        {
            list<SpriteBase*>::iterator check = it++;
            if ((*check)->IsDead())
            {
                delete *check;
                s.erase(check);
            }
            else 
            {
                (*check)->Update(sigma_time);
            }
        }
    }
        
    if (Environment::finished && !enteredFinished)
    {
        Environment::GetLayer()->gameover();
        enteredFinished = true;
    }

    
    {
        if (CCRANDOM_0_1() < 0.02)
        {
            Environment::GetSprites().push_back(new CBallSpriteWithBody());
        }
    }
    if(hero->update(delta))
    {
        //Environment::GetSprites().push_back(new CBomb(hero->getPosition().x, hero->getPosition().y));
        Environment::finished = true;
    }
    //Environment::cat = hero->getPosition();
    Environment::GetWorld()->Step(sigma_time, 8, 1);

    updateScore();
    
    sigma_time = 0;
}

void CDemoScene::draw()
{
    CCLayer::draw();
}

void CDemoScene::ccTouchesMoved(CCSet *touches, CCEvent *pEvent)
{
    //Add a new body/atlas sprite at the touched location
	CCSetIterator it;
	CCTouch* touch;
    
	for( it = touches->begin(); it != touches->end(); it++) 
	{
		touch = (CCTouch*)(*it);
        
		if(!touch)
			break;
        
		CCPoint location = touch->locationInView();
		
		location = CCDirector::sharedDirector()->convertToGL(location);
        
        CCPoint preLocation = touch->previousLocationInView();
		
		preLocation = CCDirector::sharedDirector()->convertToGL(preLocation);
        
        b2Vec2 velocity;
        float speed = 0.0001;
        
        velocity.x = - preLocation.x + location.x;
        velocity.x *= speed;
        velocity.y = - preLocation.y + location.y;
        velocity.y *= speed;
        
        {
            list<SpriteBase*> &s = Environment::GetSprites();
            for (list<SpriteBase*>::iterator it = s.begin(); it != s.end(); it++)
            {
                (*it)->LinearImpluse(velocity, location.x, location.y);
            }
        }
    }
}

void CDemoScene::gameover()
{
    {
        list<SpriteBase*> &s = Environment::GetSprites();
        for (list<SpriteBase*>::iterator it = s.begin(); it != s.end(); it++)
        {
            (*it)->playFadeOutAnimation();
        }
    }
    hero->playDieAnimation();
    
    GameOverMenuLayer* gameoverLayer = new GameOverMenuLayer();
    addChild(gameoverLayer,1000);
    gameoverLayer->release();
    
    
}


void CDemoScene::ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    
}