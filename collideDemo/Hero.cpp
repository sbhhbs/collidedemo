//
//  Hero.cpp
//  collideDemo
//
//  Created by  on 12-5-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#include "Hero.h"
#include "Environment.h"

#include "DirtSpriteWithBody.h"

#include "SimpleAudioEngine.h"

Hero::Hero(CCLayer* layerToBeAdded)
{
    sprite = CCSprite::spriteWithFile("hero.png");
    //sprite->setBlendFunc((ccBlendFunc){GL_SRC_ALPHA, GL_ONE});
    
    sprite->setScale(0.15);
    
    leftAntenna = CCSprite::spriteWithFile("antenna.png");
    rightAntenna = CCSprite::spriteWithFile("antenna.png");
    
    leftAntenna->setScale(0.6);
    rightAntenna->setScale(0.6);
    
    leftAntenna->setOpacity(200);
    rightAntenna->setOpacity(200);
    
    
    CCFiniteTimeAction* sq =  CCSequence::actions(CCFadeTo::actionWithDuration(1.0f, 140),CCFadeTo::actionWithDuration(1.0f, 200),CCFadeTo::actionWithDuration(1.0f, 140),NULL);
    
    
    leftAntenna->runAction(CCRepeat::actionWithAction(sq, 1024));
    
    
    sq =  CCSequence::actions(CCFadeTo::actionWithDuration(1.0f, 120),CCFadeTo::actionWithDuration(1.0f, 255),CCFadeTo::actionWithDuration(1.0f, 120),NULL);
    
    
    rightAntenna->runAction(CCRepeat::actionWithAction(sq, 1024));
    

    
    layerToBeAdded->addChild(leftAntenna,199);
    layerToBeAdded->addChild(rightAntenna,199);
    
    setPosition(Environment::cat);
    
    
    //sprite->setColor(ccc3(253, 192,   5));
    
    layerToBeAdded->addChild(sprite,200);
    
    CCPoint p = sprite->getPosition();
    
    b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(p.x / PTM_RATIO, p.y / PTM_RATIO);
	bodyDef.userData = sprite;
    bodyDef.fixedRotation = true;
    
    body = Environment::GetWorld()->CreateBody(&bodyDef);
    
    body->SetLinearDamping(0.9);
    
    
    b2CircleShape dynamicBox;
    // dynamicBox
    dynamicBox.m_radius =  0.26;
	
	// Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &dynamicBox;	
	fixtureDef.density = 2.0f;
	fixtureDef.friction = 0.9f;
    fixtureDef.restitution = 0.2f;
    
    //here set it to collide with the dots
    fixtureDef.filter.maskBits = 0x0002 | 0x0004 | 0x0010;
    fixtureDef.filter.categoryBits = 0x0008;
    
	body->CreateFixture(&fixtureDef);
    
    collided = false;
    
}

void Hero::playDieAnimation()
{
    leftAntenna->removeFromParentAndCleanup(true);
    rightAntenna->removeFromParentAndCleanup(true);
    
    leftAntenna = NULL;
    rightAntenna = NULL;
    
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("explode.caf"); 
    
    ccColor3B color = sprite->getColor();
    sprite->runAction(CCSpawn::actions(CCFadeTo::actionWithDuration(1.0f,128), CCScaleTo::actionWithDuration(1.0f, 5.0f),CCTintTo::actionWithDuration(1.0f, color.r, color.r, color.r),NULL));
}

Hero::~Hero()
{
    if (body)
    {
        Environment::GetWorld()->DestroyBody(body);
    }
    if (sprite)
    {
        sprite->removeFromParentAndCleanup(true);
    }
}
CCPoint Hero::getPosition()
{
    return sprite->getPosition();
}

void Hero::setPosition(CCPoint position)
{
    sprite->setPosition(position);
    if(leftAntenna)
    {
        leftAntenna->setPosition(ccpAdd(position,ccp(-13,36)));
        rightAntenna->setPosition(ccpAdd(position,ccp(8,37)));
    }
}


void Hero::setGravity(CCPoint gravity)
{
    //body->ApplyLinearImpulse(b2Vec2(gravity.x / 100, gravity.y / 100), body->GetPosition());
    body->ApplyForce(b2Vec2(gravity.x , gravity.y ), body->GetPosition());
}

void Hero::setCollide()
{
    collided = true;
}

bool Hero::update(ccTime dt)
{
    CCPoint p = CCPoint(body->GetPosition().x * PTM_RATIO, body->GetPosition().y * PTM_RATIO);
    
    CCLog("p:%f,%f",p.x,p.y);
    
    
//    const char *sources[] = {
//        "blue", "cyan", "green", "orange", "purple", "red"
//    };
//    static int turn = 0;
//    int color_type = turn;
//    
//    turn = (turn + 1 == sizeof(sources) / sizeof(char*)) ? 0 : turn + 1;
//    
//    
//    float d_x = sprite->getPosition().x + CCRANDOM_MINUS1_1() * 5;
//    float d_y = sprite->getPosition().y + CCRANDOM_MINUS1_1() * 5 - 20;
//    
//    Environment::GetSprites().push_back(
//                                        new CDirtSpriteWithBody(d_x, d_y, color_type));
//
    
    
    
    setPosition(CCPoint(body->GetPosition().x * PTM_RATIO, body->GetPosition().y * PTM_RATIO));
    
    //sprite->setRotation( -1 * CC_RADIANS_TO_DEGREES(body->GetAngle()) );    

    Environment::cat = sprite->getPosition();
    
    return collided;
}
bool Hero::isDead()
{
    
    return false;
}
