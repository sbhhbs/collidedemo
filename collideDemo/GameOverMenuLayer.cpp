//
//  GameOverMenuLayer.cpp
//  collideDemo
//
//  Created by  on 12-5-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#include "GameOverMenuLayer.h"
#include "DemoScene.h"

CCScene* GameOverMenuLayer::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::node();
    
    // add layer as a child to scene
    CCLayer* layer = new GameOverMenuLayer();
    scene->addChild(layer);
    layer->release();
    
    return scene;
}


long GameOverMenuLayer::getScore()
{
    return score;
}
void GameOverMenuLayer::setScore(long score)
{
    this->score = score;
}

static inline ccColor4B
ccc4(const GLubyte r, const GLubyte g, const GLubyte b, const GLubyte o)
{
	ccColor4B c = {r, g, b, o};
	return c;
}

void GameOverMenuLayer::ccTouchesBegan(CCSet *pTouches, CCEvent *pEvent)
{
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::transitionWithDuration(1.0f, CDemoScene::scene(),ccc3(255, 255, 255)));
    
    setIsTouchEnabled(false);
}


GameOverMenuLayer::GameOverMenuLayer()
{
    score = 0;
    
    setIsTouchEnabled( true );
    
    CCLayerColor *bk = CCLayerColor::layerWithColor(ccc4(0,0,0,255 * 0.33));
    addChild(bk);
    
    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
    
    
    char content[] = "Game Over";
    char font[] = "testFont.fnt";
    
    float offsetMultipler = 1.0f;
    
    for(int i = 0; i < 1; i++)//intended for a effect....but not beautiful = =...
    {
        CCLabelBMFont *placeHolder = CCLabelBMFont::labelWithString(content, font);
        
        placeHolder->setColor( ccc3(253, 192,   5));
        
        //placeHolder->setBlendFunc((ccBlendFunc){GL_SRC_ALPHA, GL_ONE});
        
        float xOffset,yOffset;
        xOffset = CCRANDOM_MINUS1_1();
        yOffset = CCRANDOM_MINUS1_1();
        
        placeHolder->setPosition(ccp(screenSize.width / 2 + xOffset * offsetMultipler,screenSize.height / 2+ yOffset * offsetMultipler));
        
        
        addChild(placeHolder);

    }
        
    CCLabelBMFont *touchToContinueHint = CCLabelBMFont::labelWithString("Touch to restart", font);
    
    touchToContinueHint->setColor( ccc3(253, 192,   5));
    
    touchToContinueHint->setScale(0.5);
    
    //touchToContinueHint->setBlendFunc((ccBlendFunc){GL_SRC_ALPHA, GL_ONE});
    
    float xOffset,yOffset;
    xOffset = CCRANDOM_MINUS1_1();
    yOffset = CCRANDOM_MINUS1_1();
    
    touchToContinueHint->setPosition(ccp(screenSize.width / 2 ,screenSize.height / 2 - 30));
    
    addChild(touchToContinueHint);

    
}
 

GameOverMenuLayer::~GameOverMenuLayer()
{
    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
    
}


