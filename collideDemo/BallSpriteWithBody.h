//
//  BallSpriteWithBody.h
//  collideDemo
//
//  Created by Yiguang Lu on 12-5-16.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//


#include "SpriteBase.h"

#ifndef collideDemo_BallSpriteWithBody_h
#define collideDemo_BallSpriteWithBody_h

class CBallSpriteWithBody : public SpriteBase 
{
    
public:
    CBallSpriteWithBody();
    
    virtual void Update(float dt);
    
    virtual void playFadeOutAnimation();
    virtual void fadeoutDirectly();
    
    
protected:
    virtual void SetBody(float x, float y, float dx, float dy);
    
private:
    int color_type;
    
    static int turn;
    
    b2Vec2 v;
};

#endif
