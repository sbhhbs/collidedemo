//
//  Environment.cpp
//  collideDemo
//
//  Created by Yiguang Lu on 12-5-16.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include "Environment.h"
#include "DemoScene.h"
#include "Hero.h"

b2World *Environment::world = NULL;
CDemoScene *Environment::layer = NULL;
list<SpriteBase*> Environment::sprites;
Hero *Environment::hero = NULL;
bool Environment::finished = false;

CCPoint Environment::cat;

b2World *Environment::GetWorld() 
{
    if (world == NULL) {
        world = new b2World(b2Vec2(0, 0));
    }
    
    return world;
}

CDemoScene *Environment::GetLayer() 
{
    return layer;
}

list<SpriteBase*> &Environment::GetSprites()
{
    return sprites;
}

void Environment::SetLayer(CDemoScene *l)
{
    layer = l;
}

void Environment::SetWorld(b2World *new_world)
{
    delete world;
    world = new_world;
}

