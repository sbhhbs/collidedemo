//
//  Environment.h
//  collideDemo
//
//  Created by Yiguang Lu on 12-5-16.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#include "cocos2d.h"
#include "Box2D.h"
#include "GLES-Render.h"

#include <list>

using namespace cocos2d;
using std::list;

#ifndef collideDemo_Environment_h
#define collideDemo_Environment_h

class SpriteBase;
class CDemoScene;
class Hero;
class Environment {
    
    static b2World *world;
    static CDemoScene *layer;
    static list<SpriteBase*> sprites;
    
public:
    static b2World *GetWorld();
    static CDemoScene *GetLayer();

    static void SetLayer(CDemoScene *layer);
    static void SetWorld(b2World *world);
    
    static list<SpriteBase*> &GetSprites();
    
    static bool finished;
    static CCPoint cat;
    
    static Hero *hero;
};

#define PTM_RATIO 128

#endif
