#ifndef __Menu_SCENE_H__
#define __Menu_SCENE_H__

#include "cocos2d.h"
#include <string>

using namespace cocos2d;

class Menu : public cocos2d::CCLayer
{
public:
	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	Menu();  

	// there's no 'id' in cpp, so we recommand to return the exactly class pointer
	static cocos2d::CCScene* scene();
	
	// a selector callback
	//virtual void menuCloseCallback(CCObject* pSender);
	virtual void menuPlayCallback(CCObject* pSender);
	virtual void menuHelpCallback(CCObject* pSender);
	virtual void menuAboutCallback(CCObject* pSender);
	//virtual void menuSettingCallback(CCObject* pSender);

	// implement the "static node()" method manually
	//LAYER_NODE_FUNC(Menu);

private:
	CCSprite *background;
	CCSprite *alien;
	CCSprite *glow_title;
	CCLabelBMFont *label_play;
	CCLabelBMFont *label_help;
	CCLabelBMFont *label_about;
};

#endif // __Menu_SCENE_H__
