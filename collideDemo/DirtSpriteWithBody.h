//
//  DirtSpriteWithBody.h
//  collideDemo
//
//  Created by Yiguang Lu on 12-5-16.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#include "SpriteBase.h"

#ifndef collideDemo_DirtSpriteWithBody_h
#define collideDemo_DirtSpriteWithBody_h

class CDirtSpriteWithBody : public SpriteBase 
{
public:
    CDirtSpriteWithBody();
    CDirtSpriteWithBody(float x, float y, int color_type);
    
    virtual ~CDirtSpriteWithBody() {} 
    
    virtual void Update(float dt);

protected:
    void init(float x, float y, int color_type, float scale);
    
    virtual void SetBody(float x, float y, float dx, float dy);
    
    int color_type;
    bool collide;
    
    float startFadingTime;
    
    float life;
};

#endif
