//
//  HelloWorldScene.h
//  collideDemo
//
//  Created by  on 12-5-6.
//  Copyright __MyCompanyName__ 2012年. All rights reserved.
//
#include "cocos2d.h"
#include "Box2D.h"
#include "GLES-Render.h"
#include <vector>

using namespace cocos2d;
#ifndef __HELLO_WORLD_H__
#define __HELLO_WORLD_H__

class HelloWorld : public cocos2d::CCLayer {
public:
    ~HelloWorld();
    HelloWorld();
    
    // returns a Scene that contains the HelloWorld as the only child
    static cocos2d::CCScene* scene();
    
    void initPhysicsWorld();
    
    
    // adds a new sprite at a given coordinate
    void addNewDotWithCoords(CCPoint p, ccColor3B color, float scale,b2Vec2 initVelocity);
    
    
    void addNewBallWithImage(char* pFilename, CCPoint p, b2Vec2 initVelocity);
    
    
    virtual void draw();
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent); 
    void tick(cocos2d::ccTime dt);
    void moveAtPointWithVelocity(CCPoint point,b2Vec2 velocity);
    
    virtual void update(ccTime delta);
    
    typedef struct 
    {
        CCSprite* sprite;
        b2Body *body;
        float life;
        bool collide;
    }Dot;
    
    typedef struct 
    {
        CCSprite* sprite;
        b2Body *body;
        b2Vec2 velocity;
    }Ball;
    
//    typedef struct 
//    {
//        CCSprite* sprite;
//        b2Body *body;
//        float life;
//        bool collide;
//    }LittleDot;
    
    void generateABeautifulBackground();
    
private:
    b2World* world;
    std::vector<Ball> ballVector;
    std::vector<Dot> dotVector;
    //std::vector<LittleDot> littleDotVector;
};

#endif // __HELLO_WORLD_H__
