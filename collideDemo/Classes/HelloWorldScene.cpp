//
//  HelloWorldScene.cpp
//  collideDemo
//
//  Created by  on 12-5-6.
//  Copyright __MyCompanyName__ 2012年. All rights reserved.
//
#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"

using namespace cocos2d;
using namespace CocosDenshion;

#define PTM_RATIO 32
enum 
{
	kTagTileMap = 1,
	kTagSpriteManager = 1,
	kTagAnimation1 = 1,
}; 



void HelloWorld::initPhysicsWorld()
{
    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	
    // Define the gravity vector.
	b2Vec2 gravity;
	gravity.Set(0.0f, 0.0f);
	
	// Do we want to let bodies sleep?
	bool doSleep = true;
    
	// Construct a world object, which will hold and simulate the rigid bodies.
	world = new b2World(gravity);
    world->SetAllowSleeping(doSleep); 
	world->SetContinuousPhysics(true);
    
}

void HelloWorld::generateABeautifulBackground()
{
    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
    
    for(int i = 0; i < 1000; i ++)
    {
        CCPoint p;
        p.x = screenSize.width * CCRANDOM_0_1();
        p.y = screenSize.height * CCRANDOM_0_1();
        
        this->addNewDotWithCoords(p, ccc3(48,104,192),0.3, b2Vec2(0,0));
    }
}

HelloWorld::HelloWorld()
{
	setIsTouchEnabled( true );
	setIsAccelerometerEnabled( true );
    
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	//UXLOG(L"Screen width %0.2f screen height %0.2f",screenSize.width,screenSize.height);
    
    this->initPhysicsWorld();
	
    ballVector.clear();
    //littleDotVector.clear();
    
    CCDirector::sharedDirector()->setDepthTest(false);
    
//    CCSprite *background = CCSprite::spriteWithFile("bk.jpg");
//    this->addChild(background);
//    background->setAnchorPoint(ccp(0,0));
//    background->setScale(0.8);
    
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("balls.plist");
    
    CCSpriteBatchNode *batch = CCSpriteBatchNode::batchNodeWithFile("balls.png");
        
    this->addChild(batch,100);
    
    this->scheduleUpdate();
    
    this->generateABeautifulBackground();
    
	schedule( schedule_selector(HelloWorld::tick) );
}

HelloWorld::~HelloWorld()
{
	delete world;
	world = NULL;
}

void HelloWorld::draw()
{
    CCLayer::draw();
}

void HelloWorld::update(ccTime delta)
{
    static float time = 0;
    time += delta;
    if(time > 0.1 + CCRANDOM_MINUS1_1() / 50)
    {
        time = 0;
    }
    else {
        return;
    }
    
    //ball
    {
        vector<Ball>::iterator it = ballVector.begin();
        for(; it != ballVector.end(); it++)
        {
            Ball &ball = *it;
            if(!ball.body)
                continue;
            CCSprite* sprite = ball.sprite;
            CCPoint location = sprite->getPosition();
            CCSize size = sprite->getContentSize();
            
            ball.body->SetLinearVelocity(ball.velocity);
            
            for (int i = 0; i < CCRANDOM_0_1() * 5; i++) {
                location.x += CCRANDOM_MINUS1_1() * 5;
                location.y += CCRANDOM_MINUS1_1() * 5;
                
                addNewDotWithCoords(location, ccc3(48,104,192), 1.5,b2Vec2(0, 0));
            }
        }
        //when out of screen
        for(it = ballVector.begin(); it != ballVector.end(); it++)
        {
            Ball &ball = *it;
            if(!ball.body)
                continue;
            CCSprite* sprite = ball.sprite;
            CCPoint location = sprite->getPosition();
            CCSize size = sprite->getContentSize();
            
            CCSize winSize = CCDirector::sharedDirector()->getWinSize();
            
            if(location.x + size.width / 2 < 0 || location.x - size.width / 2 > winSize.width || location.y + size.height / 2 < 0 || location.y - size.height / 2 > winSize.height)
            {
                world->DestroyBody(ball.body);
                //sprite->removeFromParentAndCleanup(true);
                ball.body = NULL;
                //sprite = NULL;
                //ballVector.erase(it);
            }            
        }

    }
    
    
    

    {
        //dot
        vector<Dot>::iterator it = dotVector.begin();
        for (; it != dotVector.end(); it++)
        {
            Dot &dot = *it;
            
            dot.life += delta;
            
            
            if(dot.life > 0.1f && !dot.collide)
            {
                for (b2Fixture* b = dot.body->GetFixtureList(); b; b = b->GetNext())
                {
                    b->SetSensor(false);
                    dot.collide = true;
                }
            }
            
            float startFadingTime = 2.0f;
            if(dot.life > startFadingTime)
            {
                CCSprite *sprite = dot.sprite;
                int opacity = 255 - (dot.life - startFadingTime) *70;
                if(opacity > 0)
                {
                    sprite->setOpacity(opacity);
                }
                else {
                    if(world->GetBodyCount() > 1)
                    {
                        world->DestroyBody(dot.body);
                        sprite->removeFromParentAndCleanup(true);
                        dotVector.erase(it);
                    }
                }
                
            }
        }
    }
}

void HelloWorld::addNewBallWithImage(char* pFilename, CCPoint p, b2Vec2 initVelocity)
{
    CCSprite *sprite;
    CCMutableArray<CCSpriteFrame*> *animationFrames = CCMutableArray<CCSpriteFrame*>::arrayWithObjects(NULL);
    
    for(int frame = 1;frame <= 30; frame++)
    {
        char framename[64];
        sprintf(framename, "%s%d.png",pFilename,frame);
        if(frame == 1)
        {
            sprite = CCSprite::spriteWithSpriteFrameName(framename);

        }
        else {
            
            animationFrames->addObject(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(framename));
        }
    }
    
    CCAnimation *animate = CCAnimation::animationWithFrames(animationFrames, 0.1);
    
    sprite->runAction(CCRepeatForever::actionWithAction(CCAnimate::actionWithAnimation(animate)));
    
    
	this->addChild(sprite,100);
	
	sprite->setPosition( CCPointMake( p.x, p.y) );
    
    
    // Define the dynamic body.
	//Set up a 1m squared box in the physics world
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(p.x/PTM_RATIO, p.y/PTM_RATIO);
	bodyDef.userData = sprite;
    bodyDef.fixedRotation = true;
    
	b2Body *body = world->CreateBody(&bodyDef);
	
    
	// Define another box shape for our dynamic body.
	b2CircleShape dynamicBox;
    // dynamicBox
    dynamicBox.m_radius =  sprite->getContentSize().height * sprite->getScaleY() / PTM_RATIO / 2;
	
	// Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &dynamicBox;	
	fixtureDef.density = 30.0f;
	fixtureDef.friction = 0.2f;
    
    
    //here set it to collide with the dots
    fixtureDef.filter.maskBits = 0x0004;
    fixtureDef.filter.categoryBits = 0x0002;
    
    
	body->CreateFixture(&fixtureDef);
    
    //body->ApplyLinearImpulse(initImpluse, body->GetPosition());
    
    
    Ball ball;
    ball.sprite = sprite;
    ball.body = body;
    ball.velocity = initVelocity;
    ballVector.push_back(ball);
}



void HelloWorld::addNewDotWithCoords(CCPoint p, ccColor3B color, float scale,b2Vec2 initVelocity)
{
	//UXLOG(L"Add sprite %0.2f x %02.f",p.x,p.y);
	CCSprite *sprite = CCSprite::spriteWithFile("ZybbleTrail.png");
    
	this->addChild(sprite,1);
	
	sprite->setPosition( CCPointMake( p.x, p.y) );
	
    sprite->setBlendFunc((ccBlendFunc){GL_SRC_ALPHA, GL_ONE});
    
    sprite->setColor(color);
    sprite->setScale(scale);
    //ccc3(48,104,192)
    
	// Define the dynamic body.
	//Set up a 1m squared box in the physics world
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(p.x/PTM_RATIO, p.y/PTM_RATIO);
	bodyDef.userData = sprite;
    bodyDef.linearDamping = 0.9;//gradually slow down
    bodyDef.angularDamping=0.3;
    bodyDef.fixedRotation = true;
    
	b2Body *body = world->CreateBody(&bodyDef);
	
    
	// Define another box shape for our dynamic body.
	b2CircleShape dynamicBox;
   // dynamicBox
    dynamicBox.m_radius = 0.1;
	
	// Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &dynamicBox;	
	fixtureDef.density = 2.5f;
	fixtureDef.friction = 0.8f;
    fixtureDef.isSensor = true;
    
    fixtureDef.filter.maskBits = 0x0002;
    fixtureDef.filter.categoryBits = 0x0004;
    
    
	body->CreateFixture(&fixtureDef);
    
    body->ApplyLinearImpulse(initVelocity, body->GetPosition());
    
    Dot dot;
    dot.sprite = sprite;
    dot.body = body;
    dot.life = 0;
    dot.collide = false;
        
    dotVector.push_back(dot);
}


void HelloWorld::tick(ccTime dt)
{
	//It is recommended that a fixed time step is used with Box2D for stability
	//of the simulation, however, we are using a variable time step here.
	//You need to make an informed choice, the following URL is useful
	//http://gafferongames.com/game-physics/fix-your-timestep/
	
	int velocityIterations = 8;
	int positionIterations = 1;
    
	// Instruct the world to perform a single step of simulation. It is
	// generally best to keep the time step and iterations fixed.
	world->Step(dt, velocityIterations, positionIterations);
	
	//Iterate over the bodies in the physics world
	for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
	{
		if (b->GetUserData() != NULL) {
			//Synchronize the AtlasSprites position and rotation with the corresponding body
			CCSprite* myActor = (CCSprite*)b->GetUserData();
			myActor->setPosition( CCPointMake( b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO) );
			myActor->setRotation( -1 * CC_RADIANS_TO_DEGREES(b->GetAngle()) );
		}	
	}
}



void HelloWorld::moveAtPointWithVelocity(CCPoint location,b2Vec2 velocity)
{
    float range = 24;
    
    b2Vec2 point;
    point.x = location.x / PTM_RATIO;
    point.y = location.y / PTM_RATIO;
    

    b2Vec2 originalVelocity = velocity;
   
    for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
    {
        velocity = originalVelocity;
        
        velocity.x *= 1 + CCRANDOM_MINUS1_1() / 0.8;
        velocity.y *= 1 + CCRANDOM_MINUS1_1() / 0.8;
        
        b2Vec2 pointLocation = b->GetPosition();
        CCPoint pLocation;
        pLocation.x = pointLocation.x * PTM_RATIO;
        pLocation.y = pointLocation.y * PTM_RATIO;
        if(((location.x - pLocation.x) * (location.x - pLocation.x) + (location.y - pLocation.y)*(location.y - pLocation.y)) < (range * range))
        {
            b->ApplyLinearImpulse(velocity, point);
        }
    }
}



void HelloWorld::ccTouchesMoved(CCSet *touches, CCEvent *pEvent)
{
    //Add a new body/atlas sprite at the touched location
	CCSetIterator it;
	CCTouch* touch;
    
	for( it = touches->begin(); it != touches->end(); it++) 
	{
		touch = (CCTouch*)(*it);
        
		if(!touch)
			break;
        
		CCPoint location = touch->locationInView();
		
		location = CCDirector::sharedDirector()->convertToGL(location);
        
        CCPoint preLocation = touch->previousLocationInView();
		
		preLocation = CCDirector::sharedDirector()->convertToGL(preLocation);
        
        b2Vec2 velocity;
        float speed = 0.1;
        
        velocity.x = - preLocation.x + location.x;
        velocity.x *= speed;
        velocity.y = - preLocation.y + location.y;
        velocity.y *= speed;

        
        this->moveAtPointWithVelocity(location, velocity);
    }
}

void HelloWorld::ccTouchesEnded(CCSet* touches, CCEvent* event)
{
	//Add a new body/atlas sprite at the touched location
//	CCSetIterator it;
//	CCTouch* touch;
//    
//	for( it = touches->begin(); it != touches->end(); it++) 
//	{
//		touch = (CCTouch*)(*it);
//        
//		if(!touch)
//			break;
//        
//		CCPoint location = touch->locationInView();
//		
//		location = CCDirector::sharedDirector()->convertToGL(location);
//        
//		addNewSpriteWithCoords( location, ccc3(48,104,192), 1.5);
//	}
    
    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	
    this->addNewBallWithImage("blue", ccp(screenSize.width * CCRANDOM_0_1(),screenSize.height * CCRANDOM_0_1()), b2Vec2(7 * CCRANDOM_MINUS1_1(), 7 * CCRANDOM_MINUS1_1()));

    
    
}

CCScene* HelloWorld::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::node();
    
    // add layer as a child to scene
    CCLayer* layer = new HelloWorld();
    scene->addChild(layer);
    layer->release();
    
    return scene;
}

