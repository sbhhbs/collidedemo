//
//  Bomb.h
//  collideDemo
//
//  Created by  on 12-5-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#include "SpriteBase.h"
#include <vector>
#ifndef collideDemo_Bomb_h
#define collideDemo_Bomb_h

class CBomb : public SpriteBase
{
public:
    CBomb(float x, float y);
    
    virtual void Update(float dt);
    
    void SetBody(float radius);
    
private:
    float m_radius;
    float x, y;
    vector<b2Body*> contactedBody;
};

#endif
