//
//  Bomb.cpp
//  collideDemo
//
//  Created by  on 12-5-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//


#include "Bomb.h"
#include "DemoScene.h"

CBomb::CBomb(float x, float y)
{
    body = NULL;
    sprite = NULL;
    m_radius = 0.5; 
    this->x = x;
    this->y = y;
    
    SetBody(m_radius);
}

void CBomb::SetBody(float radius)
{
    if (body)
    {
        Environment::GetWorld()->DestroyBody(body);
    }
    
    b2BodyDef bodyDef;
	bodyDef.type = b2_staticBody;
	bodyDef.position.Set(x / PTM_RATIO, y / PTM_RATIO);
	bodyDef.userData = sprite;
    bodyDef.fixedRotation = true;
    
    body = Environment::GetWorld()->CreateBody(&bodyDef);
    
    // Define another box shape for our dynamic body.
	b2CircleShape dynamicBox;
    // dynamicBox
    dynamicBox.m_radius = radius;
	
	// Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &dynamicBox;	
	fixtureDef.density = 50.0f;
	fixtureDef.friction = 0.8f;
    fixtureDef.restitution = 1.0f;
    
    fixtureDef.filter.maskBits = 0x0006;
    fixtureDef.filter.categoryBits = 0x0008;
    
    
	body->CreateFixture(&fixtureDef);
}

void CBomb::Update(float dt)
{
    for(b2ContactEdge* e = body->GetContactList();e;e = e->next)
    {
        if(find(contactedBody.begin(),contactedBody.end(),e->other) != contactedBody.end())
        {
            e->contact->SetEnabled(false);
            continue;
        }   
//        b2Vec2 v = e->other->GetLinearVelocity();
//        v.x = -v.x * 5;
//        v.y = -v.y * 5;
//        e->other->SetLinearVelocity(v);
        contactedBody.push_back(e->other);
        
        b2Vec2 hitVector;
        float hitForce;
        float distance;
        
        b2Body* effectBody = e->other;
        
        bool isBall = true;
        for (b2Fixture *b = e->other->GetFixtureList(); b; b = b->GetNext())
        {
            b2Filter filter = b->GetFilterData();
            if(filter.maskBits == 0x0002)
            {
                isBall = false;
                break;
            }
        }
        if(!isBall)
        {
            continue;
        }
        
        
        
        b2Vec2 bodyPos = effectBody->GetWorldCenter();
        b2Vec2 pos;
        pos.x = x;
        pos.y = y;
        
        hitVector = (bodyPos - pos);
        distance = hitVector.Normalize(); //Makes a 1 unit length vector from HitVector, while getting the length.
        hitForce=(m_radius - distance)*0.1;//TODO: This is linear, but that's not realistic.
        if(hitForce > 0)
        {
            effectBody->ApplyLinearImpulse(hitForce * hitVector, effectBody->GetWorldCenter());
        
            // e->contact->SetEnabled(false);
            for (b2Fixture *b = e->other->GetFixtureList(); b; b = b->GetNext())
            {
                b->SetSensor(true);
            }
        }
        //        
    }
    m_radius += dt * 2;
    SetBody(m_radius);

    if (m_radius > 1.5)
    {
    }
    
    if (m_radius > 2)
    {
        isDead = true;
        Environment::finished = true;
    }
}