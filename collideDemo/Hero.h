//
//  Hero.h
//  collideDemo
//
//  Created by  on 12-5-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#ifndef collideDemo_Hero_h
#define collideDemo_Hero_h

#include "Box2D.h"
#include "cocos2d.h"

using namespace cocos2d;

class Hero
{
public:
    Hero(CCLayer* layerToBeAdded);
    virtual ~Hero();
    
    
    CCPoint getPosition();
    bool update(ccTime dt);
    bool isDead();
    void setGravity(CCPoint gravity);
    void setCollide();
    void playDieAnimation();
private:
    void setPosition(CCPoint position);
    CCSprite* sprite;
    CCSprite *leftAntenna;
    CCSprite *rightAntenna;
    b2Body *body;
    bool collided;
};

#endif
