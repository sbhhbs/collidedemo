//
//  AtomSpriteWithBody.h
//  collideDemo
//
//  Created by  on 12-5-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#include "DirtSpriteWithBody.h"

#ifndef collideDemo_AtomSpriteWithBody_h
#define collideDemo_AtomSpriteWithBody_h

class CAtomSpriteWithBody : public CDirtSpriteWithBody
{
public:
    CAtomSpriteWithBody();
    
    virtual ~CAtomSpriteWithBody();
};

#endif
