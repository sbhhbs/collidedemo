//
//  DirtSpriteWithBody.cpp
//  collideDemo
//
//  Created by Yiguang Lu on 12-5-16.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include "DirtSpriteWithBody.h"

/*
    const char *sources[] = {
        "blue", "cyan", "green", "orange", "purple", "red"
    };
*/

ccColor3B colors[] = 
{
    ccc3( 48, 104, 192), // blue
    ccc3(157, 242, 242), // cyan
    ccc3(104, 192,  48), // green
    ccc3(253, 192,   5), // orange
    ccc3(193,  66, 205), // purple
    ccc3(192,  48, 104)  // red
};

CDirtSpriteWithBody::CDirtSpriteWithBody() : SpriteBase()
{
    float w = CCDirector::sharedDirector()->getWinSize().width;
    float h = CCDirector::sharedDirector()->getWinSize().height;
    
    init(w * CCRANDOM_0_1(), h * CCRANDOM_0_1(), sizeof(colors) / sizeof(ccColor3B) * CCRANDOM_0_1(), 0.3);
}

CDirtSpriteWithBody::CDirtSpriteWithBody(float x, float y, int color_type) : SpriteBase(), startFadingTime(.5f)
{
    init(x, y, color_type, 1.5);
}

void CDirtSpriteWithBody::init(float x, float y, int color_type, float scale)
{
    SetSprite("ZybbleTrail.png", scale);
    
    this->color_type = color_type;
    sprite->setColor(colors[color_type]);
    
    if(fadeoutOnInit)
        fadeoutDirectly();
    
    if (scale == 1.5)
    {
        SetBody(x, y, 0, 0);
    }
    else 
    {
        SetBody(x, y, CCRANDOM_MINUS1_1() / 2048, 0);
    }
}

void CDirtSpriteWithBody::SetBody(float x, float y, float dx, float dy)
{
    SpriteBase::SetBody(x, y, dx, dy);
    
    body->SetLinearDamping(0.99);
    body->SetAngularDamping(0.3);
    
  	// Define another box shape for our dynamic body.
	b2CircleShape dynamicBox;
    // dynamicBox
    dynamicBox.m_radius = 0.01;
	
	// Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &dynamicBox;	
	fixtureDef.density = 5.0f;
	fixtureDef.friction = 0.8f;
    fixtureDef.isSensor = true;
    
    fixtureDef.filter.maskBits = 0x0002 | 0x0008;
    fixtureDef.filter.categoryBits = 0x0004;
    
    
	body->CreateFixture(&fixtureDef);
    
    for (b2Fixture *b = body->GetFixtureList(); b; b = b->GetNext())
    {
        b->SetSensor(true);
    }
    collide = false;
    
    body->ApplyLinearImpulse(b2Vec2(dx, dy), body->GetPosition());
}

void CDirtSpriteWithBody::Update(float dt)
{
    SpriteBase::Update(dt);
    
    life += dt;
    
    if (!collide && life > 0.1)
    {
        for (b2Fixture *b = body->GetFixtureList(); b; b = b->GetNext())
        {
            b->SetSensor(false);
        }
        collide = true;
    }  
    
    if(life > startFadingTime)
    {
        int opacity = 255 - (life - startFadingTime) * 50 * sprite->getScaleX();
        if(opacity > 0)
        {
            sprite->setOpacity(opacity);
        }
        else 
        {
            isDead = true;
        }
    }
}