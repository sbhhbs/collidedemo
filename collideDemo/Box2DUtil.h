//
//  Box2DUtil.h
//  collideDemo
//
//  Created by  on 12-5-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#ifndef collideDemo_Box2DUtil_h
#define collideDemo_Box2DUtil_h

#include "Box2D.h"
class Box2DUtils
{
public:
    static void PhysExplosion(b2World *world, const b2Vec2 &pos, float radius, float force);    
};

#endif
