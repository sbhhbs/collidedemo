//
//  BallSpriteWithBody.cpp
//  collideDemo
//
//  Created by Yiguang Lu on 12-5-16.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#include "BallSpriteWithBody.h"
#include "DirtSpriteWithBody.h"
#include "Environment.h"
#include "Hero.h"

CBallSpriteWithBody::CBallSpriteWithBody() : SpriteBase() {
    
    float w = CCDirector::sharedDirector()->getWinSize().width;
    float h = CCDirector::sharedDirector()->getWinSize().height;
    
    float x, y, dx, dy;
    
    if (CCRANDOM_0_1() < 0.5) {
        x = (CCRANDOM_0_1() < 0.5) ? 0 : w;
        y = h * CCRANDOM_0_1();
    }
    else {
        x = w * CCRANDOM_0_1();
        y = (CCRANDOM_0_1() < 0.5) ? 0: h;
    }
    
    dx = (Environment::cat.x - x) / w * 5;
    if (dx > 0)
    {
        dx += 2;
    }
    else 
    {
        dx -= 2;
    }
    dy = (Environment::cat.y - y) / h * 5;
    if (dy > 0)
    {
        dy += 2;
    }
    else 
    {
        dy -= 2;
    }
    
    dx /= 3.5;
    dy /= 3.5;
    
    
    const char *sources[] = {
        "blue", "cyan", "green", "orange", "purple", "red"
    };
    color_type = turn;

    turn = (turn + 1 == sizeof(sources) / sizeof(char*)) ? 0 : turn + 1;
    
    SetSpriteWithAnimation(sources[color_type]);
    
    sprite->setPosition(CCPoint(x, y));
    
    SetBody(x, y, dx, dy);
    
    if(fadeoutOnInit)
        fadeoutDirectly();
}

int CBallSpriteWithBody::turn = 0;

void CBallSpriteWithBody::playFadeOutAnimation()
{
    sprite->runAction(CCFadeOut::actionWithDuration(1.0f));
    fadeoutOnInit = true;
}
void CBallSpriteWithBody::fadeoutDirectly()
{
    sprite->setOpacity(0);
}


void CBallSpriteWithBody::SetBody(float x, float y, float dx, float dy) {
    
    SpriteBase::SetBody(x, y, dx, dy);
    
    // Define another box shape for our dynamic body.
	b2CircleShape dynamicBox;
    // dynamicBox
    dynamicBox.m_radius =  sprite->getContentSize().height * sprite->getScaleY() / PTM_RATIO / 2;
	
	// Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &dynamicBox;	
	fixtureDef.density = 30.0f;
	fixtureDef.friction = 0.2f;
    fixtureDef.restitution = 0.2f;
    
    //here set it to collide with the dots
    fixtureDef.filter.maskBits = 0x0004 | 0x0008;
    fixtureDef.filter.categoryBits = 0x0002;
    
	body->CreateFixture(&fixtureDef);
    //body->ApplyLinearImpulse(b2Vec2(dx, dy), body->GetPosition());
    v = b2Vec2(dx, dy);
    body->SetLinearVelocity(v);
}

void CBallSpriteWithBody::Update(float dt)
{
    SpriteBase::Update(dt);
    
    if(fadeoutOnInit)
        return;
    
    
    float x = sprite->getPosition().x;
    float y = sprite->getPosition().y;
    
    float w = CCDirector::sharedDirector()->getWinSize().width;
    float h = CCDirector::sharedDirector()->getWinSize().height;

    if (CCRANDOM_0_1() < 0.6 + 0.45 * (v.x * v.x + v.y * v.y) / 25)
    {
        for (int i = 0; i < 3; i++)
        {
            float d_x = x + CCRANDOM_MINUS1_1() * 5;
            float d_y = y + CCRANDOM_MINUS1_1() * 5;
            
            if (d_x < w && d_x > 0 && d_y < h && d_y > 0) 
            {
                Environment::GetSprites().push_back(
                    new CDirtSpriteWithBody(d_x, d_y, color_type));
            }
        }
    }
    
    CCPoint heroPos,myPos;
    heroPos = (Environment::hero)->getPosition();
    myPos = sprite->getPosition();
    
    if((heroPos.x - myPos.x) * (heroPos.x - myPos.x) + (heroPos.y - myPos.y) * (heroPos.y - myPos.y) < 3800)
    {
        Environment::hero->setCollide();
    }
    
//    b2Vec2 v = body->GetLinearVelocity();
    //body->SetLinearVelocity(v);
}