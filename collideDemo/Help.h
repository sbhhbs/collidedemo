#ifndef __Help_SCENE_H__
#define __Help_SCENE_H__

#include "cocos2d.h"
#include <string>

using namespace cocos2d;

class Help : public cocos2d::CCLayer
{
public:
	Help();  

	static cocos2d::CCScene* scene();

	virtual void menuBackCallback(CCObject* pSender);

private:
	CCSprite *background;
	CCSprite *alien;
	CCSprite *help_text;

	CCLabelBMFont *label_back;

};
#endif