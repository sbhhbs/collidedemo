//
//  DemoScene.h
//  collideDemo
//
//  Created by Yiguang Lu on 12-5-16.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//
#include "cocos2d.h"
#include "Box2D.h"
#include "Hero.h"
#include <vector>

using namespace cocos2d;
#ifndef collideDemo_DemoScene_h
#define collideDemo_DemoScene_h



class CDemoScene : public CCLayer
{
public:
    static CCScene *scene();
    
    CDemoScene(); 
    
    virtual ~CDemoScene();
    
    virtual void update(ccTime delta);
    
    virtual void draw();
    
    virtual void ccTouchesMoved(CCSet *touches, CCEvent *pEvent);
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
   
    virtual void didAccelerate(CCAcceleration* pAccelerationValue);
        
    virtual void menuBackCallback(CCObject* pSender);
    
    
protected:
    void updateAccelerometer(ccTime dt);
    
    void gameover();
    
    void addWalls();
    
    void updateScore();
private:
    CCPoint playerVelocity;
    Hero *hero;
    bool enteredFinished;
    CCLabelBMFont *scoreLabel;
    long score;
};

#endif
