//
//  SpriteBase.h
//  collideDemo
//
//  Created by Yiguang Lu on 12-5-16.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#include "Environment.h"

#ifndef collideDemo_SpriteBase_h
#define collideDemo_SpriteBase_h

class SpriteBase {
    
public:
    virtual void Update(float dt);
    bool IsDead();
    
    virtual ~SpriteBase();
    
    void LinearImpluse(b2Vec2 velocity, float x, float y);
    
    virtual void playFadeOutAnimation();
    virtual void fadeoutDirectly();
    static void initFadeOutParameter();
    
protected:
    virtual bool IsOutOfScreen();
    
    CCSprite *sprite;
    b2Body *body;
    
    SpriteBase();
    
    void SetSpriteWithAnimation(const char *frameSymbol);
    void SetSprite(const char *resourceName, float scale = 1);
    
    virtual void SetBody(float x, float y, float dx, float dy);
    
    bool isDead;
    static bool fadeoutOnInit;
};

#endif
