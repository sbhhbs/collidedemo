#include "Help.h"
#include "Menu.h"
#include "cocos2d.h"

using namespace cocos2d;

CCScene* Help::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::node();
    
    // add layer as a child to scene
    CCLayer* layer = new Help();
    scene->addChild(layer);
    layer->release();
    
    return scene;
}


Help::Help()
{
	//////////////////////////////
	// 1. super init first
	
	/////////////////////////////
	CCSize size = CCDirector::sharedDirector()->getWinSize();
	background = CCSprite::spriteWithFile("bkg7.png");
	background->setPosition(ccp(size.width*0.70, size.height/2));
	background->setScale(0.8);
	this->addChild(background);

	alien = CCSprite::spriteWithFile("alien2.png");
	alien->setPosition(ccp(size.width*0.70,size.height*0.6));
	alien->setScale(1.1);
	alien->setOpacity(230);
	this->addChild(alien);

	help_text = CCSprite::spriteWithFile("help-text.png");
	help_text->setPosition(ccp(size.width*0.25,size.height*0.4));
	//help_text->setScale(1);
	this->addChild(help_text);

	label_back = CCLabelBMFont::labelWithString("BACK","testFont.fnt");
	label_back->setColor(ccc3(207,136,88));
	label_back->setScale(0.6);
	//this->addChild(label_back);

	CCMenuItemLabel *pBackItem = CCMenuItemLabel::itemWithLabel(
		label_back,
		this,
		menu_selector(Help::menuBackCallback) );
	
	CCMenu* pMenu = CCMenu::menuWithItems(pBackItem, NULL);
	pMenu->setPosition(ccp(size.width-50,size.height-30));
	this->addChild(pMenu, 1);
}

void Help::menuBackCallback(CCObject* pSender)
{
	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::transitionWithDuration(0.7f,Menu::scene()));
}
