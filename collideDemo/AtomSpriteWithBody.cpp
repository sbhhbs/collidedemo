//
//  AtomSpriteWithBody.cpp
//  collideDemo
//
//  Created by  on 12-5-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include "AtomSpriteWithBody.h"

CAtomSpriteWithBody::CAtomSpriteWithBody() : CDirtSpriteWithBody()
{
    startFadingTime = 1.0f + 8 * CCRANDOM_0_1();
    body->SetLinearDamping(0.0);
    
    
//    CCSprite *anotherSprite = CCSprite::spriteWithFile("ZybbleTrail.png");
//	
//    anotherSprite->setBlendFunc((ccBlendFunc){GL_SRC_ALPHA, GL_ONE});
//    
//    anotherSprite->setColor(sprite->getColor());
//
//    anotherSprite->setPosition(<#const cocos2d::CCPoint &pos#>)
//    sprite->addChild(anotherSprite);
//    
//    
    
    //Environment::GetLayer()->addChild(sprite, 1);
    
}

CAtomSpriteWithBody::~CAtomSpriteWithBody()
{
    Environment::GetSprites().push_back(new CAtomSpriteWithBody());
}