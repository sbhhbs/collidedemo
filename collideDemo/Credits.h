#ifndef __Credit_SCENE_H__
#define __Credit_SCENE_H__
#include "cocos2d.h"
#include <string>

using namespace cocos2d;
class Credits : public cocos2d::CCLayer
{
public:
	Credits();  

	static cocos2d::CCScene* scene();

	virtual void menuBackCallback(CCObject* pSender);

	//LAYER_NODE_FUNC(Credits);

private:
	CCSprite *background;
	//CCSprite *alien;
	CCSprite *credits_text;


	CCLabelBMFont *label_back;

};
#endif