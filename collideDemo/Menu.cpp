#include "Menu.h"
#include "Help.h"
#include "Credits.h"
#include "DemoScene.h"
#include "cocos2d.h"

#include "SimpleAudioEngine.h"

using namespace cocos2d;

CCScene* Menu::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::node();
    
    // add layer as a child to scene
    CCLayer* layer = new Menu();
    scene->addChild(layer);
    layer->release();
    
    return scene;
}

// on "init" you need to initialize your instance
Menu::Menu()
{
	if(!CocosDenshion::SimpleAudioEngine::sharedEngine()->isBackgroundMusicPlaying())
    {
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("reaction song.mp3",true);
    }

    
	/////////////////////////////

	CCSize size = CCDirector::sharedDirector()->getWinSize();
	background = CCSprite::spriteWithFile("bkg7.png");
	background->setPosition(ccp(size.width*0.7, size.height/2));
	background->setScale(0.8);
	this->addChild(background);
	alien = CCSprite::spriteWithFile("alien.png");
	alien->setPosition(ccp(size.width/4,size.height*0.35));
	alien->setRotation(-10);
	alien->setOpacity(230);
	this->addChild(alien);

	glow_title = CCSprite::spriteWithFile("glowTitle.png");
	glow_title ->setPosition(ccp(400,size.height-130));
	glow_title->setScale(0.8);
	this->addChild(glow_title);

	label_play = CCLabelBMFont::labelWithString("PLAY","testFont.fnt");
	//label_play->setPosition(560,360);
	label_play->setColor(ccc3(106,66,154));


	label_about = CCLabelBMFont::labelWithString("CREDITS","testFont.fnt");
	//label_about->setPosition(260,360);
	label_about->setColor(ccc3(120,149,85));
	label_about->setScale(0.6);


	label_help = CCLabelBMFont::labelWithString("HELP","testFont.fnt");
	//label_help->setPosition(260,300);
	label_help->setColor(ccc3(131,80,65));
	label_help->setScale(0.6);

	//CCMenuItemImage *pCloseItem = CCMenuItemImage::itemFromNormalImage(
	//									"CloseNormal.png",
	//									"CloseSelected.png",
	//									this,
	//									menu_selector(Menu::menuCloseCallback) );

	CCMenuItemLabel *pPlayItem = CCMenuItemLabel::itemWithLabel(
		label_play,
		this,
		menu_selector(Menu::menuPlayCallback) );

	CCMenuItemLabel *pHelpItem = CCMenuItemLabel::itemWithLabel(
		label_help,
		this,
		menu_selector(Menu::menuHelpCallback) );

	CCMenuItemLabel *pAboutItem = CCMenuItemLabel::itemWithLabel(
		label_about,
		this,
		menu_selector(Menu::menuAboutCallback) );

	//pCloseItem->setPosition( ccp(CCDirector::sharedDirector()->getWinSize().width - 20, 20) );
	pPlayItem->setPosition( ccp(560,360) );
	pHelpItem->setPosition( ccp(860,160) );
	pAboutItem->setPosition( ccp(800,360) );

	// create menu, it's an autorelease object
	CCMenu* pMenu = CCMenu::menuWithItems( pPlayItem,pHelpItem,pAboutItem, NULL);
	pMenu->setPosition( CCPointZero );
	this->addChild(pMenu, 1);
	
}


void Menu::menuPlayCallback(CCObject* pSender)
{
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::transitionWithDuration(0.7f,CDemoScene::scene()));
}

void Menu::menuHelpCallback(CCObject* pSender)
{
	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::transitionWithDuration(0.7f,Help::scene()));
}

void Menu::menuAboutCallback(CCObject* pSender)
{
	CCDirector::sharedDirector()->replaceScene(CCTransitionFade::transitionWithDuration(0.7,Credits::scene()));
}

