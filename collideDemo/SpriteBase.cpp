//
//  SpriteBase.cpp
//  collideDemo
//
//  Created by Yiguang Lu on 12-5-16.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#include "SpriteBase.h"
#include "DemoScene.h"

bool SpriteBase::fadeoutOnInit = false;

SpriteBase::SpriteBase() : isDead(false)
{
//    Environment::GetSprites().push_back(this);
}

void SpriteBase::initFadeOutParameter()
{
    fadeoutOnInit = false;
}


void SpriteBase::SetSprite(const char *resourceName, float scale) {
    
    sprite = CCSprite::spriteWithFile("ZybbleTrail.png");
	
    sprite->setBlendFunc((ccBlendFunc){GL_SRC_ALPHA, GL_ONE});

    sprite->setScale(scale);
    
    Environment::GetLayer()->addChild(sprite, 1);
}


void SpriteBase::playFadeOutAnimation()
{
    if(sprite)
    {
        ccColor3B color = sprite->getColor();
        sprite->runAction(CCTintTo::actionWithDuration(1.0f, color.r, color.r, color.r));
        fadeoutOnInit = true;
    }
}
void SpriteBase::fadeoutDirectly()
{
    ccColor3B color = sprite->getColor();
    sprite->setColor(ccc3(color.r,color.r,color.r));
}


void SpriteBase::SetSpriteWithAnimation(const char *frameSymbol) {
    
    CCMutableArray<CCSpriteFrame*> *animationFrames = CCMutableArray<CCSpriteFrame*>::arrayWithObjects(NULL);
    
    for(int frame = 1;frame <= 30; frame++)
    {
        char framename[64];
        sprintf(framename, "%s%d.png",frameSymbol,frame);
        if(frame == 1)
        {
            sprite = CCSprite::spriteWithSpriteFrameName(framename);
            
        }
        else {
            
            animationFrames->addObject(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(framename));
        }
    }
    
    CCAnimation *animate = CCAnimation::animationWithFrames(animationFrames, 0.1);
    
    sprite->runAction(CCRepeatForever::actionWithAction(CCAnimate::actionWithAnimation(animate)));

    Environment::GetLayer()->addChild(sprite, 100);
}

void SpriteBase::SetBody(float x, float y, float dx, float dy) {
    
    b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(x / PTM_RATIO, y / PTM_RATIO);
	bodyDef.userData = sprite;
    bodyDef.fixedRotation = true;
    
    body = Environment::GetWorld()->CreateBody(&bodyDef);
}

void SpriteBase::Update(float dt)
{
    if (IsOutOfScreen())
    {
        isDead = true;
    }

    sprite->setPosition(CCPoint(body->GetPosition().x * PTM_RATIO, body->GetPosition().y * PTM_RATIO));
    
    sprite->setRotation( -1 * CC_RADIANS_TO_DEGREES(body->GetAngle()) );
}

bool SpriteBase::IsOutOfScreen()
{
    float w = CCDirector::sharedDirector()->getWinSize().width;
    float h = CCDirector::sharedDirector()->getWinSize().height;
    
    float x = body->GetPosition().x;
    float y = body->GetPosition().y;
    
    float r = sprite->getContentSize().width / 2;
    
    return !(x < w + r && x > 0 - r && y < h + r && y > 0 - r);
}

bool SpriteBase::IsDead()
{
    return isDead;
}

SpriteBase::~SpriteBase()
{
    if (body)
    {
        Environment::GetWorld()->DestroyBody(body);
    }
    if (sprite)
    {
        sprite->removeFromParentAndCleanup(true);
    }
}

void SpriteBase::LinearImpluse(b2Vec2 velocity, float px, float py)
{
    
    float x = body->GetPosition().x * PTM_RATIO;
    float y = body->GetPosition().y * PTM_RATIO;
    float range = 36;
    float distanceSquare = (range * range - (px - x) * (px - x) - (py - y) * (py - y)) / range / range;
    
    if (distanceSquare > 0)
    {
        velocity.x *= 1 + CCRANDOM_0_1() / 0.8 * distanceSquare;
        velocity.y *= 1 + CCRANDOM_0_1() / 0.8 * distanceSquare;
        
        body->ApplyLinearImpulse(velocity, b2Vec2(px / PTM_RATIO, py / PTM_RATIO));
    }
}