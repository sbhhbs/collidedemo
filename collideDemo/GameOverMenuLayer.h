//
//  GameOverMenuLayer.h
//  collideDemo
//
//  Created by  on 12-5-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#ifndef collideDemo_GameOverMenuLayer_h
#define collideDemo_GameOverMenuLayer_h


#include "CCLayer.h"
#include "cocos2d.h"

using namespace cocos2d;

class GameOverMenuLayer : public CCLayer
{
public:
    long getScore();
    void setScore(long score);
    
    static CCScene *scene();
    
    GameOverMenuLayer(); 
    
    virtual ~GameOverMenuLayer();
    
    virtual void ccTouchesBegan(CCSet *pTouches, CCEvent *pEvent);

    
private:
    long score;
    CCLabelBMFont *userScoreLabel;
    CCLabelBMFont *bestScoreLabel;
};

#endif
